﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public GameObject ShotSpawn_Mode1;
    public GameObject ShotSpawn_Mode2;
    public GameObject ShotSpawn_Mode3;
    public GameObject BulletPrefab;
    public Image[] ShieldImages;
    public Image[] BoostsImages;
    public List<TemporaryBoostType> ActiveBoosts = new List<TemporaryBoostType>();

    [SerializeField]
    float _shipSpeed = 2;
    [SerializeField]
    float _fireRate = 0.25f;
    [SerializeField]
    int _shipWeaponsDamage = 1;
    [SerializeField]
    float _shieldRegenerationRate = 4f;
    [SerializeField]
    bool _invicibility = false;

    int _shieldCharge = 4;
    bool _gunEnabled = true;
    bool _gamePaused = false;
    Animator _animation;
    GameController _gameController;
    Coroutine[] BonusEnding = new Coroutine[5];

    public int ShieldCharge
    {
        get { return _shieldCharge; }
        set
        {
            _shieldCharge = value;
            UpdateShieldUI();
        }
    }
    ShipWeaponsMode _fireMode = ShipWeaponsMode.SingleShot;
    public ShipWeaponsMode WeaponsMode
    {
        get { return _fireMode; }
        set 
        { 
            _fireMode = value;
            GameObject.FindGameObjectWithTag("GameController").GetComponent<BonusesManager>().playerWeaponMode = _fireMode;
        }
    }

    private void Start()
    {
        _animation = GetComponentInChildren<Animator>();
        _gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();

        StartCoroutine(ShieldGenerator());
    }

    private void Update()
    {
        PlayerMovement();
        PlayerShooting();
        PlayerOtherControls();
        PlayerCheats();
    }

    #region Shield Related
    private IEnumerator ShieldGenerator()
    {
        while (true)
        {
            yield return new WaitForSeconds(_shieldRegenerationRate);

            if (ShieldCharge < 4)
            {
                ShieldCharge++;
            }
        }
    }

    private void UpdateShieldUI()
    {
        if (_shieldCharge == 0)
        {
            ShieldImages[0].enabled = false;
        }
        if (_shieldCharge == 1)
        {
            ShieldImages[0].enabled = true;
            ShieldImages[1].enabled = false;
        }
        if (_shieldCharge == 2)
        {
            ShieldImages[1].enabled = true;
            ShieldImages[2].enabled = false;
        }
        if (_shieldCharge == 3)
        {
            ShieldImages[2].enabled = true;
            ShieldImages[3].enabled = false;
        }
        if (_shieldCharge == 4)
        {
            ShieldImages[3].enabled = true;
        }
    }
    #endregion
    #region Movement Related
    private void PlayerShooting()
    {
        if (Input.GetButton("Jump"))
        {
            if (_gunEnabled && !_gamePaused)
            {
                switch (WeaponsMode)
                {
                    case ShipWeaponsMode.SingleShot:
                        var shot = Instantiate(BulletPrefab, ShotSpawn_Mode1.transform.position, Quaternion.identity);
                        shot.GetComponent<ShotController>().ShotDamage = _shipWeaponsDamage;
                        break;
                    case ShipWeaponsMode.DoubleShot:
                        var shot21 = Instantiate(BulletPrefab,
                            ShotSpawn_Mode2.transform.GetChild(0).position,
                            Quaternion.identity);
                        shot21.GetComponent<ShotController>().ShotDamage = _shipWeaponsDamage;
                        var shot22 = Instantiate(BulletPrefab,
                            ShotSpawn_Mode2.transform.GetChild(1).position,
                            Quaternion.identity);
                        shot22.GetComponent<ShotController>().ShotDamage = _shipWeaponsDamage;
                        break;
                    case ShipWeaponsMode.FastTripleShot:
                        StartCoroutine(FastTripleShot());
                        break;
                    case ShipWeaponsMode.TripleAngleShot:
                        var shot31 = Instantiate(BulletPrefab,
                            ShotSpawn_Mode3.transform.GetChild(0).position,
                            Quaternion.Euler(0, 0, -10));
                        shot31.GetComponent<ShotController>().ShotDamage = _shipWeaponsDamage;
                        var shot32 = Instantiate(BulletPrefab,
                            ShotSpawn_Mode3.transform.GetChild(1).position,
                            Quaternion.Euler(0, 0, 10));
                        shot32.GetComponent<ShotController>().ShotDamage = _shipWeaponsDamage;
                        var shot33 = Instantiate(BulletPrefab,
                            ShotSpawn_Mode3.transform.GetChild(2).position,
                            Quaternion.identity);
                        shot33.GetComponent<ShotController>().ShotDamage = _shipWeaponsDamage;
                        break;
                    default:
                        break;
                }

                _gunEnabled = false;
                StartCoroutine(GunCooldown());
            }
        }
    }

    private IEnumerator FastTripleShot()
    {
        GameObject shot;

        for (int i = 0; i < 2; i++)
        {
            shot = Instantiate(BulletPrefab, ShotSpawn_Mode1.transform.position, Quaternion.identity);
            shot.GetComponent<ShotController>().ShotDamage = _shipWeaponsDamage;

            yield return new WaitForSeconds(_fireRate / 4);
        }
    }

    private IEnumerator GunCooldown()
    {
        yield return new WaitForSeconds(_fireRate);
        _gunEnabled = true;
    }

    private void PlayerMovement()
    {
        Vector3 movement = Vector3.zero;
        var verticalInput = Input.GetAxisRaw("Horizontal");
        
        movement = new Vector3(verticalInput * Time.deltaTime * _shipSpeed, 0f, 0f);

        if (transform.localPosition.x < 3.8f & transform.localPosition.x > -3.8f)
        {
            transform.Translate(movement);
        }
        else
        {
            if (transform.localPosition.x < 3.8f)
            {
                transform.localPosition =  new Vector3(-3.79f, -4.125f, 0f);
            }
            else
            {
                transform.localPosition = new Vector3(3.79f, -4.125f, 0f);
            }
        }
    }

    private void PlayerOtherControls()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (_gameController != null)
            {
                _gameController.PauseUnpauseGame();
                _gamePaused = !_gamePaused;
            }
        }
    }
    #endregion

    #region TemporaryBoosts Related
    public void ApplyTemporaryBonus(TemporaryBoostType boostType)
    {
        if (ActiveBoosts.Count(x => x == boostType) >= 2)
        {
            return;
        }

        StartCoroutine(StartTemporaryBonus(boostType));
    }

    private  IEnumerator StartTemporaryBonus(TemporaryBoostType boost)
      {
        var boostType = boost;

        switch (boostType)
        {
            case TemporaryBoostType.FireRateBuff:
                ActiveBoosts.Add(TemporaryBoostType.FireRateBuff);
                if (BonusEnding[0] != null)
                {
                    StopCoroutine(BonusEnding[0]);
                }
                BoostsImages[0].enabled = true;
                _fireRate /= 2;
                break;
            case TemporaryBoostType.Invicibility:
                ActiveBoosts.Add(TemporaryBoostType.Invicibility);
                if (BonusEnding[1] != null)
                {
                    StopCoroutine(BonusEnding[1]);
                }
                BoostsImages[1].enabled = true;
                _invicibility = true;
                break;
            case TemporaryBoostType.ShieldRecharge:
                ActiveBoosts.Add(TemporaryBoostType.ShieldRecharge);
                if (BonusEnding[2] != null)
                {
                    StopCoroutine(BonusEnding[2]);
                }
                BoostsImages[2].enabled = true;
                _shieldRegenerationRate /= 2;
                break;
            case TemporaryBoostType.SpeedBuff:
                ActiveBoosts.Add(TemporaryBoostType.SpeedBuff);
                if (BonusEnding[3] != null)
                {
                    StopCoroutine(BonusEnding[3]);
                }
                BoostsImages[3].enabled = true;
                _shipSpeed *= 1.5f;
                break;
            case TemporaryBoostType.DamageBuff:
                ActiveBoosts.Add(TemporaryBoostType.DamageBuff);
                if (BonusEnding[4] != null)
                {
                    StopCoroutine(BonusEnding[4]);
                }
                BoostsImages[4].enabled = true;
                _shipWeaponsDamage++;
                break;
            default:
                break;
        }

        yield return new WaitForSeconds(25f);

        switch (boostType)
        {
            case TemporaryBoostType.FireRateBuff:
                ActiveBoosts.Remove(TemporaryBoostType.FireRateBuff);
                if (ActiveBoosts.Where(x => x == TemporaryBoostType.FireRateBuff).Count() == 0)
                {
                    BonusEnding[0] = StartCoroutine(BonusEndingAnim(TemporaryBoostType.FireRateBuff));
                }
                break;
            case TemporaryBoostType.Invicibility:
                ActiveBoosts.Remove(TemporaryBoostType.Invicibility);
                if (ActiveBoosts.Where(x => x == TemporaryBoostType.Invicibility).Count() == 0)
                {
                    BonusEnding[1] = StartCoroutine(BonusEndingAnim(TemporaryBoostType.Invicibility));
                }
                break;
            case TemporaryBoostType.ShieldRecharge:
                ActiveBoosts.Remove(TemporaryBoostType.ShieldRecharge);
                if (ActiveBoosts.Where(x => x == TemporaryBoostType.ShieldRecharge).Count() == 0)
                {
                    BonusEnding[2] = StartCoroutine(BonusEndingAnim(TemporaryBoostType.ShieldRecharge));
                }
                break;
            case TemporaryBoostType.SpeedBuff:
                ActiveBoosts.Remove(TemporaryBoostType.SpeedBuff);
                if (ActiveBoosts.Where(x => x == TemporaryBoostType.SpeedBuff).Count() == 0)
                {
                    BonusEnding[3] = StartCoroutine(BonusEndingAnim(TemporaryBoostType.SpeedBuff));
                }
                break;
            case TemporaryBoostType.DamageBuff:
                ActiveBoosts.Remove(TemporaryBoostType.DamageBuff);
                if (ActiveBoosts.Where(x => x == TemporaryBoostType.DamageBuff).Count() == 0)
                {
                    BonusEnding[4] = StartCoroutine(BonusEndingAnim(TemporaryBoostType.DamageBuff));
                }
                break;
            default:
                break;
        }

        yield return new WaitForSeconds(5f);

        switch (boostType)
        {
            case TemporaryBoostType.FireRateBuff:
                if (!ActiveBoosts.Contains(boostType))
                {
                    if (BonusEnding[0] != null)
                    {
                        StopCoroutine(BonusEnding[0]);
                    }
                    BoostsImages[0].enabled = false;
                }
                _fireRate *= 2;
                break;
            case TemporaryBoostType.Invicibility:
                if (!ActiveBoosts.Contains(boostType))
                {
                    if (BonusEnding[1] != null)
                    {
                        StopCoroutine(BonusEnding[1]);
                    }
                    BoostsImages[1].enabled = false;
                    _invicibility = false;
                }
                break;
            case TemporaryBoostType.ShieldRecharge:
                if (!ActiveBoosts.Contains(boostType))
                {
                    if (BonusEnding[2] != null)
                    {
                        StopCoroutine(BonusEnding[2]);
                    }
                    BoostsImages[2].enabled = false;
                }
                _shieldRegenerationRate *= 2;
                break;
            case TemporaryBoostType.SpeedBuff:
                if (!ActiveBoosts.Contains(boostType))
                {
                    if (BonusEnding[3] != null)
                    {
                        StopCoroutine(BonusEnding[3]);
                    }
                    BoostsImages[3].enabled = false;
                }
                _shipSpeed /= 1.5f;
                break;
            case TemporaryBoostType.DamageBuff:
                if (!ActiveBoosts.Contains(boostType))
                {
                    if (BonusEnding[4] != null)
                    {
                        StopCoroutine(BonusEnding[4]);
                    }
                    BoostsImages[4].enabled = false;
                }
                _shipWeaponsDamage--;
                break;
            default:
                break;
        }
    }

    private IEnumerator BonusEndingAnim(TemporaryBoostType boost)
    {
        var boostType = boost;

        while (true)
        {
            switch (boostType)
            {
                case TemporaryBoostType.FireRateBuff:
                    BoostsImages[0].enabled = !BoostsImages[0].enabled;
                    break;
                case TemporaryBoostType.Invicibility:
                    BoostsImages[1].enabled = !BoostsImages[1].enabled;
                    break;
                case TemporaryBoostType.ShieldRecharge:
                    BoostsImages[2].enabled = !BoostsImages[2].enabled;
                    break;
                case TemporaryBoostType.SpeedBuff:
                    BoostsImages[3].enabled = !BoostsImages[3].enabled;
                    break;
                case TemporaryBoostType.DamageBuff:
                    BoostsImages[4].enabled = !BoostsImages[4].enabled;
                    break;
                default:
                    break;
            }

            yield return new WaitForSeconds(0.5f);
        }
    }
    #endregion

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("EnemyBullet") ||
            collision.gameObject.CompareTag("Enemy"))
        {
            if (!_invicibility)
            {
                if (ShieldCharge > 0)
                {
                    _animation.SetTrigger("ShieldAnim");
                    ShieldCharge--;
                }
                else
                {
                    GameManager.instance.GameOver();
                }
            }
        }
    }

    private void PlayerCheats()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            if (WeaponsMode != ShipWeaponsMode.TripleAngleShot)
            {
                WeaponsMode++;
            }
        }
    }
}

public enum ShipWeaponsMode
{
    SingleShot = 1,
    DoubleShot = 2,
    FastTripleShot = 3,
    TripleAngleShot = 4,
}