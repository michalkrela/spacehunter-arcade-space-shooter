﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    [Header("Music Related")]
    public GameObject MusicOff;
    public GameObject MusicOn;
    public AudioSource Music;

    [Header("Gameplay Related")]
    public GameObject ScoreText;
    public GameObject HighscoreText;
    public GameObject StartBtn;

    [Header("Canvases")]
    public Canvas MainCanvas;
    public Canvas CreditsCanvas;

    public int CurrentGameScore;

    int _highScore = 0;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }

        SceneManager.sceneLoaded += SceneSwitchingEvent;
    }

    private void Update()
    {
        if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                StartGame();
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    #region Buttons Commands
    public void StartGame()
    {
        if (HighscoreText != null)
        {
            HighscoreText.GetComponent<TMP_Text>().enabled = false;
        }
        SceneManager.LoadScene(1);
    }

    public void ShowHideCredits()
    {
        if (CreditsCanvas.enabled == false)
        {
            MainCanvas.enabled = false;
            CreditsCanvas.enabled = true;
        }
        else
        {
            CreditsCanvas.enabled = false;
            MainCanvas.enabled = true;
        }
    }

    public void ToggleMusic()
    {
        if (Music.isPlaying)
        {
            Music.Stop();
            MusicOff.SetActive(false);
            MusicOn.SetActive(true);
        }
        else
        {
            Music.Play();
            MusicOff.SetActive(true);
            MusicOn.SetActive(false);
        }
    }
    #endregion
    private void FindMusicBtnsReferences()
    {
        MusicOff = GameObject.Find("PauseCanvas/SoundToggleBtn/MusicOff");
        MusicOff.GetComponent<Button>().onClick.AddListener(ToggleMusic);
        MusicOn = GameObject.Find("PauseCanvas/SoundToggleBtn/MusicOn");
        MusicOn.GetComponent<Button>().onClick.AddListener(ToggleMusic);
        if (Music.isPlaying)
        {
            MusicOn.SetActive(false);
        }
        else
        {
            MusicOff.SetActive(false);
        }
    }

    private void SceneSwitchingEvent(Scene scene, LoadSceneMode sceneMode)
    {
        if (scene.buildIndex == 1)
        {
            FindMusicBtnsReferences();
        }

        if (scene.buildIndex == 2)
        {
            if (ScoreText == null)
            {
                ScoreText = GameObject.FindGameObjectWithTag("ScoreText");
            }
            if (HighscoreText == null)
            {
                HighscoreText = GameObject.FindGameObjectWithTag("NewHighScoreText");
            }
            if (StartBtn == null)
            {
                StartBtn = GameObject.FindGameObjectWithTag("StartGameBtn");
                StartBtn.GetComponent<Button>().onClick.AddListener(delegate () { StartGame(); });
            }

            ScoreText.GetComponent<TMP_Text>().text = string.Format("Score: {0}", CurrentGameScore);

            if (CurrentGameScore > _highScore)
            {
                _highScore = CurrentGameScore;
                HighscoreText.GetComponent<TMP_Text>().enabled = true;
            }
        }
    }

    public void GameOver()
    {
        SceneManager.LoadScene(2);
    }
}