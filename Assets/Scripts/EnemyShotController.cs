﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShotController : MonoBehaviour
{
    [SerializeField]
    float _shotSpeed = 10f;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Rigidbody2D>().AddForce(transform.up * _shotSpeed, ForceMode2D.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        SelfDestruct();
    }

    private void SelfDestruct()
    {
        if (transform.localPosition.y < -5.5f ||
            transform.localPosition.x > 3.8f ||
            transform.localPosition.x < -3.8f)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject);
        }
    }
}
