﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemyData", menuName = "Enemy Data", order = 51)]
public class EnemyBlueprint : ScriptableObject
{
    [SerializeField]
    string _name;
    public string Name { get => _name; set => _name = value; }

    [SerializeField]
    Sprite _sprite;
    public Sprite Sprite { get => _sprite; set => _sprite = value; }

    [SerializeField]
    Color _spriteColor;
    public Color SpriteColor { get => _spriteColor; set => _spriteColor = value; }

    [SerializeField]
    int _scoreValue;
    public int ScoreValue { get => _scoreValue; set => _scoreValue = value; }

    [SerializeField]
    int _difficultyValue;
    public int DifficultyValue { get => _difficultyValue; set => _difficultyValue = value; }

    [SerializeField]
    int _health;
    public int Health { get => _health; set => _health = value; }

    [SerializeField]
    float _fireRate;
    public float FireRate { get => _fireRate; set => _fireRate = value; }

    [SerializeField]
    float _speed;
    public float Speed { get => _speed; set => _speed = value; }

    [SerializeField]
    ShipWeaponsMode _weaponMode;
    public ShipWeaponsMode WeaponMode { get => _weaponMode; set => _weaponMode = value; }
}
