﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BonusesManager : MonoBehaviour
{
    public CollectibleBonus[] Bonuses;
    public ShipWeaponsMode playerWeaponMode = ShipWeaponsMode.SingleShot;

    public CollectibleBonus GenerateBonus()
    {
        if (UnityEngine.Random.Range(1, 100) <= 20)
        {
            switch (UnityEngine.Random.Range(1,100))
            {
                case var n when n >= 1 && n <= 40:
                    var scorePointsBonuses = Bonuses.Where
                        (x => x.BonusType == BonusType.ScorePoints).ToArray();
                    return scorePointsBonuses[UnityEngine.Random.Range(0, scorePointsBonuses.Length)];
                case var n when n >= 41 && n <= 55:
                    return Bonuses.FirstOrDefault(x => x.BonusType == BonusType.ShieldCharge);
                case var n when n > 56:
                    if (playerWeaponMode == ShipWeaponsMode.TripleAngleShot)
                    {
                        var tempBoostBonuses = Bonuses.Where
                            (x => x.BonusType == BonusType.TemporaryBoost).ToArray();
                        return tempBoostBonuses[UnityEngine.Random.Range(0, tempBoostBonuses.Length)];
                    }
                    else
                    {
                        return Bonuses.FirstOrDefault(x => x.BonusType == BonusType.WeaponsBonus);
                    }
                default:
                    return null;
            }
        }
        else
        {
            return null;
        }
    }
}
