﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CollectibleBonusData", menuName = "Collectible Bonus Data", order = 51)]
public class CollectibleBonus : ScriptableObject
{
    [SerializeField]
    string _name;
    public string Name { get => _name; set => _name = value; }

    [SerializeField]
    BonusType _bonusType;
    public BonusType BonusType { get => _bonusType; set => _bonusType = value; }

    [SerializeField]
    Sprite _icon;
    public Sprite Icon { get => _icon; set => _icon = value; }

    [SerializeField]
    Color _backgroundColor;
    public Color BackgroundColor { get => _backgroundColor; set => _backgroundColor = value; }

    [SerializeField]
    int _bonusValue;
    public int BonusValue { get => _bonusValue; set => _bonusValue = value; }

    [SerializeField]
    TemporaryBoostType _temporaryBonusType;
    public TemporaryBoostType TemporaryBonusType { get => _temporaryBonusType; set => _temporaryBonusType = value; }
}

public enum BonusType
{
    ShieldCharge,
    ScorePoints,
    WeaponsBonus,
    TemporaryBoost
}

public enum TemporaryBoostType
{
    FireRateBuff = 0,
    Invicibility = 1,
    SpeedBuff = 2,
    DamageBuff = 3,
    ShieldRecharge = 4,
}
