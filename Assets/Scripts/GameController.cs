﻿using System;
using System.Collections;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public Canvas PauseCanvas;
    public TMP_Text ScoreText;
    public GameObject BonusScoreText;
    public GameObject EnemyPrefab;
    public EnemyBlueprint[] EnemyData;
    public GameObject[] EnemySpawners;
    public int Score;
    [SerializeField]
    float _enemySpawnTime = 4f;

    int _currentDifficultyLevel = 10;

    // Start is called before the first frame update
    void Awake()
    {
        Score = 0;
        StartCoroutine(WaveManager());
    }

    private IEnumerator WaveManager()
    {
        while (true)
        {
            StartCoroutine(EnemySpawner(_currentDifficultyLevel));
            _currentDifficultyLevel += 10;

            yield return new WaitForSeconds(10f);
        }
    }

    private IEnumerator EnemySpawner(int difficultyPointsPerWave)
    {
        int currentDifficultyPoints = 0;
        while (currentDifficultyPoints < difficultyPointsPerWave)
        {
            yield return new WaitForSeconds(_enemySpawnTime);

            var enemy = Instantiate(EnemyPrefab, EnemySpawners[UnityEngine.Random.Range(0, EnemySpawners.Length)].transform.position, Quaternion.identity);
            var enemyPrefab = GenerateEnemy(difficultyPointsPerWave - currentDifficultyPoints);

            currentDifficultyPoints += enemyPrefab.DifficultyValue;
            enemy.GetComponent<EnemyController>().Setup(enemyPrefab);
        }
        Debug.LogFormat("Wave value: {0}", currentDifficultyPoints);
    }

    private EnemyBlueprint GenerateEnemy(int diffPointsLeftInWave)
    {
        var availableEnemies = EnemyData.Where(x => x.DifficultyValue <= diffPointsLeftInWave).ToArray();

        return availableEnemies[UnityEngine.Random.Range(0, availableEnemies.Length)];
    }

    public void AddScore (int score)
    {
        Score += score;
        ScoreText.text = String.Format("Score: {0}", Score);

        GameManager.instance.CurrentGameScore = Score;
    }
    public void AddBonusScore(int bonusValue)
    {
        BonusScoreText.GetComponent<TMP_Text>().text = string.Format("+{0}", bonusValue);
        BonusScoreText.GetComponent<Animator>().SetTrigger("BonusScore");
        AddScore(bonusValue);
    }

    public void PauseUnpauseGame()
    {
        if (Time.timeScale == 1)
        {
            Time.timeScale = 0;
            PauseCanvas.enabled = true;
        }
        else
        {
            Time.timeScale = 1;
            PauseCanvas.enabled = false;
        }
    }
}