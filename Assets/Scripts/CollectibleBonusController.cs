﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CollectibleBonusController : MonoBehaviour
{
    public TMP_Text FloatingTextPrefab;
    public SpriteRenderer Icon;
    private CollectibleBonus _bonusData;
    public CollectibleBonus BonusData
    {
        get { return _bonusData; }
        set
        {
            _bonusData = value;
            if (_bonusData != null)
            {
                Icon.sprite = BonusData.Icon;
                GetComponent<SpriteRenderer>().color = BonusData.BackgroundColor;
            }
        }
    }

    [SerializeField]
    float _fallSpeed = 0.75f;

    GameController _gameController;

    private void Awake()
    {
        _gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();

        GetComponent<Rigidbody2D>().AddForce(Vector2.down * _fallSpeed, ForceMode2D.Impulse);
    }

    private void ShowFloatingText()
    {
        switch (BonusData.BonusType)
        {
            case BonusType.ShieldCharge:
            case BonusType.ScorePoints:
                FloatingTextPrefab.text = string.Format("+{0}", BonusData.BonusValue);
                break;
            case BonusType.WeaponsBonus:
                FloatingTextPrefab.text = string.Format("Upgrade!");
                break;
            case BonusType.TemporaryBoost:
                switch (BonusData.TemporaryBonusType)
                {
                    case TemporaryBoostType.FireRateBuff:
                        FloatingTextPrefab.text = string.Format("Fire Rate Boost!");
                        break;
                    case TemporaryBoostType.Invicibility:
                        FloatingTextPrefab.text = string.Format("Invicibility Boost!");
                        break;
                    case TemporaryBoostType.SpeedBuff:
                        FloatingTextPrefab.text = string.Format("Ship Speed Boost!");
                        break;
                    case TemporaryBoostType.DamageBuff:
                        FloatingTextPrefab.text = string.Format("Weapon Damage Boost!");
                        break;
                    case TemporaryBoostType.ShieldRecharge:
                        FloatingTextPrefab.text = string.Format("Shield Recharge Boost!");
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }

        FloatingTextPrefab.color = BonusData.BackgroundColor;  

        Destroy(Instantiate(FloatingTextPrefab, transform.position, Quaternion.identity).gameObject, 1f);
    }

    // Update is called once per frame
    void Update()
    {
        SelfDestruct();
    }

    private void SelfDestruct()
    {
        if (transform.localPosition.y < -5.5f)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            bool showFloatingText = true;
            Destroy(gameObject);
            

            switch (BonusData.BonusType)
            {
                case BonusType.ShieldCharge:
                    if (collision.gameObject.GetComponent<PlayerController>().ShieldCharge < 4)
                    {
                        collision.gameObject.GetComponent<PlayerController>().ShieldCharge
                            += BonusData.BonusValue;
                    }
                    else
                    {
                        _gameController.AddBonusScore(100);
                        showFloatingText = false;
                    }
                    break;
                case BonusType.ScorePoints:
                    _gameController.AddBonusScore(BonusData.BonusValue);
                    break;
                case BonusType.WeaponsBonus:
                    if (collision.gameObject.GetComponent<PlayerController>().WeaponsMode != ShipWeaponsMode.TripleAngleShot)
                    {
                        collision.gameObject.GetComponent<PlayerController>().WeaponsMode++;
                    }
                    else
                    {
                        _gameController.AddBonusScore(100);
                        showFloatingText = false;
                    }
                    break;
                case BonusType.TemporaryBoost:
                    collision.gameObject.GetComponent<PlayerController>().ApplyTemporaryBonus(BonusData.TemporaryBonusType);
                    break;
                default:
                    break;
            }

            if (showFloatingText)
            {
                ShowFloatingText();
            }
        }
    }
}