﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public GameObject ExplosionAnim;
    public GameObject ShotSpawn_Mode1;
    public GameObject ShotSpawn_Mode2;
    public GameObject BulletPrefab;
    public GameObject BonusPrefab;
    public EnemyBlueprint EnemyData;
    public ParticleSystem LaserDamageParticles;
    public CollectibleBonus HeldBonus;

    GameController _gameController;
    BonusesManager _bonusesManager;
    Coroutine _activeTickingLaserDmg;

    int _actualHealth;

    ShipWeaponsMode _fireMode = ShipWeaponsMode.SingleShot;
    public ShipWeaponsMode WeaponsMode
    {
        get { return _fireMode; }
        set { _fireMode = value; }
    }


    public void Setup(EnemyBlueprint enemyData)
    {
        EnemyData = enemyData;
        _actualHealth = enemyData.Health;
        WeaponsMode = EnemyData.WeaponMode;

        var sprite = GetComponentInChildren<SpriteRenderer>();
        sprite.sprite = enemyData.Sprite;
        sprite.color = enemyData.SpriteColor;
    }

    void Start()
    {
        var gameController = GameObject.FindGameObjectWithTag("GameController");
        _gameController = gameController.GetComponent<GameController>();
        _bonusesManager = gameController.GetComponent<BonusesManager>();
        LaserDamageParticles.Stop();

        HeldBonus = _bonusesManager.GenerateBonus();

        GetComponent<Rigidbody2D>().AddForce(Vector2.down * EnemyData.Speed, ForceMode2D.Impulse);

        StartCoroutine(Shot());
    }

    private IEnumerator Shot()
    {
        while (true)
        {
            yield return new WaitForSeconds(UnityEngine.Random.Range(EnemyData.FireRate, EnemyData.FireRate + 0.5f));

            switch (WeaponsMode)
            {
                case ShipWeaponsMode.SingleShot:
                    Instantiate(BulletPrefab, ShotSpawn_Mode1.transform.position, Quaternion.Euler(0, 0, 180));
                    break;
                case ShipWeaponsMode.DoubleShot:
                    Instantiate(BulletPrefab,
                        ShotSpawn_Mode2.transform.GetChild(0).position,
                        Quaternion.Euler(0, 0, 180));
                    Instantiate(BulletPrefab,
                        ShotSpawn_Mode2.transform.GetChild(1).position,
                        Quaternion.Euler(0, 0, -165));
                    Instantiate(BulletPrefab,
                        ShotSpawn_Mode2.transform.GetChild(2).position,
                        Quaternion.Euler(0, 0, -180));
                    Instantiate(BulletPrefab,
                        ShotSpawn_Mode2.transform.GetChild(3).position,
                        Quaternion.Euler(0, 0, 165));
                    Instantiate(BulletPrefab,
                        ShotSpawn_Mode2.transform.GetChild(4).position,
                        Quaternion.Euler(0, 0, 180));
                    break;
                default:
                    break;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        SelfDestruct();
    }

    private void SelfDestruct()
    {
        if (transform.localPosition.y < -5.5f)
        {
            Destroy(gameObject);
        }
    }

    private void TakeDamage(int damageValule)
    {
        _actualHealth -= damageValule;
        if (_actualHealth <= 0)
        {
            DestroyShip();
        }
    }

    private void DestroyShip()
    {
        _gameController.AddScore(EnemyData.ScoreValue);

        Destroy(gameObject);
        if (HeldBonus != null)
        {
            var bonusObject = Instantiate(BonusPrefab, transform.position, quaternion.identity);
            bonusObject.GetComponent<CollectibleBonusController>().BonusData = HeldBonus;
        }

        SpawnExplosionAnimation();
    }

    private void SpawnExplosionAnimation()
    {
        Destroy(Instantiate(ExplosionAnim, transform.position, quaternion.identity), 1.8f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            DestroyShip();
        }

        if (collision.gameObject.CompareTag("PlayerBullet"))
        {
            TakeDamage(collision.gameObject.GetComponent<ShotController>().ShotDamage);
        }
    }
}